// const { defineConfig } = require("@vue/cli-service");
module.exports = {
    css: {
        loaderOptions: {
            scss: {
                additionalData: `
                @use 'sass:math';
                @import "@/assets/css/variables.scss";
                @import "@/assets/css/mixins.scss";
            `,
            },
        },
    },
    transpileDependencies: true,
};
