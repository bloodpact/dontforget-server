import { createRouter, createWebHistory } from "vue-router";
import BuyList from "@/views/BuyList.vue";
import Recipes from "@/views/Recipes.vue";

const routes = [
    {
        path: "/",
        name: "BuyList",
        component: BuyList,
    },
    {
        path: "/recipes",
        name: "recipes",
        component: Recipes,
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
