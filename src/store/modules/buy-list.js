const buylist = {
    namespaced: true,
    state: () => ({
        buylist: [],
    }),
    mutations: {
        CLEAR(state) {
            state.buylist = [];
        },
        ADD_BUY(state, buy) {
            state.buylist = [...state.buylist, buy];
        },
        DELETE_BUY(state, buy) {
            state.buylist = state.buylist.filter((el) => el.name != buy);
        },
    },
    actions: {},
    getters: {
        buylist: (s) => s.buylist,
    },
};

export default buylist;
