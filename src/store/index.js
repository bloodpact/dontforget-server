import { createStore } from "vuex";
import { modules } from "./modules";

const store = createStore({
    modules,
    state: {},
    getters: {},
    mutations: {},
    actions: {},
});
export default store;
